FROM ubuntu
RUN apt-get upgrade && apt-get update && apt-get install -y ruby-full
COPY main.rb /usr/src/main.rb
COPY array_gen.csv /usr/src/array_gen.csv
WORKDIR /usr/src/ 
CMD ruby main.rb 
