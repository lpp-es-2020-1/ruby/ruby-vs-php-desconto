# O csv foi gerado com esse código
require 'csv'

# define a quantidade de arrays que serão gerados
qtd = 50 

# cria o arquivo com arrays
CSV.open("array_gen.csv", "w") do |csv|

    # repete a criação de arrays aleatórios até atingir a quantidade definida
    for i in 0..(qtd-1) do
        
        # gera um array de tamanho aleatório entre 1 e 20
        csv << Array.new(rand(1..20)) { |j| j = ((rand(20.00..101.99)).round(2))}        
    end
end