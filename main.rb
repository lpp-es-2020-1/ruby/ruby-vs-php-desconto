def desconto (valoresProdutos)

  #cria um array ordenado dos produtos
  prodEmOrdem = valoresProdutos.sort

  #inicializa váriaveis
  prodEmDesc = []
  desc = 0
  valoresProdutosRtrn = []

  #separa os produtos que serão descontados
  prodEmDesc = prodEmOrdem.slice(0, valoresProdutos.size/3) 
  
  #soma o valor total do desconto 
  desc = prodEmDesc.inject(0){|sum,x| sum + x } 

  #divide o valor total do desconto entre todos os produtos, arredonda e coloca no array que irá retornar
  valoresProdutosRtrn = valoresProdutos.collect{|x| (x - (desc/valoresProdutos.size)).round(2)}

  return valoresProdutosRtrn 
end

#puxa a biblioteca csv
require 'csv' 

#cria uma array de arrays com o arquivo
tabela = CSV.parse(File.read("array_gen.csv"), converters: :numeric)

#testa cada um dos arrays no arquivo e escreve o resultado na tela
print ("\n Resultados:")
for i in 0..(tabela.count-1) do
  print ("\n array #{i+1}: #{desconto(tabela[i])}")
end
