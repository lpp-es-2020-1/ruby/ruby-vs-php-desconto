# Comparando Legibilidade

Legibilidade é definido como o julgamento humano sobre o quão fácil um texto é lido.
Em engenharia de software a legibilidade é definida como o julgamento de um desenvolvedor sobre quão fácil um código fonte é de entender, trabalhar colaborativamente e manter. Por mais que exista consenso em quão importante legibilidade é para a qualidade de software, não existe consenso sobre quais fatores afetam a legibilidade de código, eu escolhi um modelo proposto em um artigo para essa comparação mas o resultado não é um teste definitivo em relação a legibilidade.


### Esse artigo foi a primeira proposta de um modelo formal para calcular a legibilidade de código.
### (https://web.eecs.umich.edu/~weimerw/p/weimer-tse2010-readability-preprint.pdf)

## Sobre o artigo

O “Learning a Metric for Code Readability”, descreve um modelo para entender a legibilidade de código, com base em uma análise onde 120 pessoas participaram, cada participante leu 100 snippets (snippets não trechos curtos) de código e avaliou de 1 a 5 cada um deles quanto a legibilidade.

A principal forma de julgamento usada na hora de analisar os dados foi não analisar a pontuação absoluta, já que os participantes estão julgando o snippet de forma subjetiva (sem uma instrução específica sobre como julgar), por exemplo se um participante deu nota 5 para um snippet e outro participante deu nota 4 e esses participantes deram respectivamente nota 2 e 1 para outro snippet, os dois participantes concordam que o primeiro snippet tem a legibilidade melhor do que o segundo. Achei importante mencionar isso, pois pra mim isso reforça como as métricas resultantes dessa pesquisa são baseadas no julgamento humano.

Os pesquisadores usaram machine learning para entender as correlações as características do código e o julgamento dos participantes, depois disso eles tentam relacionar os resultados com ferramentas de bug-fix, complexidade e o ciclo de vida de um software, mas o mais importante para explicar como Ruby tem uma boa legibilidade é o poder de predição do modelo. Os autores do artigo inclusive mencionam como o gráfico que mostra o poder de predição do modelo ensina bastante sobre legibilidade de código, na parte 6 do artigo os autores discutem como certos fatores afetam a legibilidade segundo o modelo. 

## Resultados e correlação

Para começar, os pesquisadores concluíram que o tamanho dos identificadores quase não interfere na legibilidade do código. Essa observação contradiz a crença comum de que "identificadores de somente um caractere tornam a manutenção mais difícil". Uma observação que talvez tenha contribuído para um movimento significativo em direção à "auto documentação código ”, que geralmente é caracterizado por nomes de identificação longos e descritivos e poucas observações.
 
### Correlação com Ruby

    Ruby é muito flexível em relação ao isso, identificadores podem ter qualquer tamanho e podem ser nomeados com caracteres alfabéticos, números e underscore(‘_’).

Os comentários são uma forma direta de comunicar a intenção do código. Espera-se que sua presença aumente drasticamente a legibilidade. Mas segundo os dados, os comentários foram apenas moderadamente bem correlacionados com a noção de legibilidade tiveram 33% no gráfico do poder de predição.

### Correlação com Ruby

    Acho que isso é irrelevante para comparar a legibilidade de ruby e php já que as duas linguagens suportam comentários, usar ou não comentário nesse caso é escolha do programador.

O número de identificadores e caracteres por linha tem uma forte influência na métrica de legibilidade de 100% e 96% de poder de predição, respectivamente. Parece que, assim como frases longas são mais difíceis de entender, assim são linhas de código longas. O resultado apoia o conceito que diz que os programadores devem manter suas linhas curtas, mesmo que isso signifique dividir uma instrução em várias linhas.

### Correlação com Ruby

    Para esse teste o código em PHP precisa ser escrito usando os mesmos identificadores, sem comentários e o mesmo array que eu usei em Ruby.

    O código em Ruby tem 20 linhas 474 caracteres, 9 identificadores.(identificadores: valoresProdutosRtrn, valoresProdutos, prodEmOrdem, prodEmDesc, valoresProdutosRtrn, desc, x, b, desconto) Em média o código em ruby tem 0.45 identificadores e 23.7 caracteres. 
    Segundo o modelo o código com menor média de caracteres e identificadores deve ter a legibilidade melhor.

Nos experimentos, a importância da contagem de caracteres por linha sugere que as linguagens devem favorecer o uso de construções com instruções que encorajam linhas curtas. Os dados sugerem que as linguagens devem adicionar palavras-chave se isso significar que os programas podem ser escritos com menos novos identificadores. 

### Correlação com Ruby

    Nesse caso Ruby quanto PHP tem por exemplo métodos como sum e sort para simplificar o entendimento e melhorar a produtividade.


## Problemas que os autores do artigo encontraram com esse modelo

Para terminar, é importante dizer que esse modelo de legibilidade é descritivo em vez de normativo ou prescritivo, por tanto embora possa ser usado para prever julgamentos de legibilidade, não pode ser interpretado diretamente para prescrever alterações que irão melhorar a legibilidade. Por exemplo, o número médio de linhas em branco é apresentado como um aspecto que melhora a legibilidade segundo o modelo, só de adicionar 5 linhas em branco seguidas a legibilidade do código já se torna alta, isso acontece porque linhas em branco são usadas para organizar o código e separar funções por exemplo. Outro problema com a métrica gerada é que Identificadores longos pioram a legibilidade e substituir todos os identificadores por sequências aleatórias de duas letras melhoram a legibilidade segundo o modelo. Esses são problemas já identificados nesse modelo que irão ajudar os autores a refinar o próximo modelo de legibilidade em seus próximos trabalhos.


## Tabela

Essa Tabela é baseada na tabela no artigo que mostra o inidice de predição do modelo.

### As Setas indicam se a característica está positivamente (para cima) ou negativamente (para baixo) correlata com a legibilidade, e a quantidade de setas indica o poder preditivo (3 setas = "alto", 2 setas = "médio", 1 seta = "baixo").

| Recurso                                                                                                                       | Relevancia |
|-------------------------------------------------------------------------------------------------------------------------------|-------|
| Quantidade de caracteres por linha <br>(quantidade de caracteres dividido pela quantidade de inhas)                                                                                           |  ⇩⇩⇩  |
| Quantidade de identificadores por linha <br>(média: quantidade de identificadores dividido pela quantidade de linhas)                                                                                                |  ⇩⇩⇩  |
| Indentação antes de espaço em branco por linha <br>(quantidade de blocos compostos por código e espaço em branco dividido pela quantidade de linhas) |   ⇩⇩  |
| Quantidade de keywords por linha <br>(quantidade de keywords dividido pela quantidade de linhas)                                                                                                       |   ⇩⇩  |
| Quantidade de números por linha <br>(quantidade de algarismos dividido pela quantidade de linhas)                                                                                                        |   ⇩   |
| Quantidade de parêntesis por linha <br>(quantidade de parêntesis, dividido pela quantidade de linhas)                                                                                                     |  ⇩⇩⇩  |  
| Quantidade de pontos por linha <br>(quantidade de pontos dividido pela quantidade de linhas)                                                                                                         |  ⇩⇩⇩  |   
| Quantidade de linhas em branco por linha <br>(quantidade de linhas em branco dividido pela quantidade de linhas)                                                                                               |   ⇧⇧  |    
| Quantidade de espaços por linha <br>(quantidade de espaços dividido pela quantidade de linhas)                                                                                                        |   ⇩   |      
| Quantidade de assignment por linha <br>(quantidade de assignments dividido pela quantidade de linhas)                                                                                                     |   ⇩   |      
| Quantidade de branches(if) por linha <br>(quantidade de branches dividido pela quantidade de linhas)                                                                                                   |   ⇩   |      
| Quantidade de loops (for,while) por linha <br>(quantidade de loops dividido pela quantidade de linhas)                                                                                              |   ⇩   |      
| Quantidade de operadores aritméticos por linha <br>(quantidade de operadores aritméticos dividido pela quantidade de linhas)                                                                                        |   ⇧   |       
| Quantidade de operadores de comparação por linha <br>(quantidade de operadores de comparação dividido pela quantidade de linhas)                                                                                       |   ⇩   | 


#### Eu proponho que façamos uma tabela expondo o código feito no desafio, a tabela deverá conter o calculo de cada recurso especifico, mas excluindo os recursos que foram marcados com somente uma seta pois o modelo não consegue prever com acurácia se esse fator afeta legibilidade, para diminuir o fator “programador” a fim de comparar as linguagens pelo que elas são, preciso que o código que vai ser comparado não tenha comentários, e que ambos os códigos usem os mesmos nomes nos identificadores e o mesmo array.
Na tabela de referência quando a seta está para baixo quer dizer que código com o menor quantidade desse recurso tem a legibilidade melhor, os recursos com a seta para cima tem melhor legibilidade quando esse recurso é encontrado em maior quantidade, a quantidade de setas demonstra o quão bem o modelo consegue prever a legibilidade com base em um recurso específico, os recursos com somente uma seta tem pouca relevância na comparação pois o modelo não consegue prever a legibilidade de forma confiável baseado nesses recursos, e os recursos com duas setas tem menor relevância do que os recursos com três setas.

### Tabela Ruby:
O código em Ruby sem os comentários tem
20 linhas, 474 caracteres, 9 identificadores,
7 blocos de indentação antes de espaço em branco,
12 keywords, 16 parêntesis ou 8 pares de parêntesis,
14 pontos e 8 linhas em branco.   

| Recurso                                                                                                                       |Média|Relevância|
|-------------------------------------------------------------------------------------------------------------------------------|-------|-------|
| Quantidade de caracteres por linha                                                                                            |23.7|⇩⇩⇩|
| Quantidade de identificadores por linha                                                                                       |0.45|⇩⇩⇩|
| Indentação antes de espaço em branco por linha                                                                                |0.35|⇩⇩|
| Quantidade de keywords por linha                                                                                              |0.6|⇩⇩|
| Quantidade de parêntesis por linha                                                                                            |0.8|⇩⇩⇩|  
| Quantidade de pontos por linha                                                                                                |0.7|⇩⇩⇩|   
| Quantidade de Linhas em Branco por linha                                                                                      |0.4|⇧⇧|






## Extras:

Eu pensei em várias ideias para fazer esse trabalho, li vários artigos, e escrevi alguns comentários sobre esses artigos antes de definir que essa seria a proposta do meu trabalho, eu estava pensando em comentar os artigos que eu lí e correlacionar-los com as linguagens, mas eu achei que algumas métricas não encaixariam bem no trabalho, só que eu também não queria me desfazer do que eu escrevi sobre estes artigos, então nessa parte eu vou deixar alguns comentários sobre alguns trabalhos eu li sobre legibilidade, esses trabalhos são mais recentes do que o artigo que eu usei e propõem modelos mais completos de legibilidade, mas eu concluí que o artigo que eu usei era o único que apresentava uma forma de comparar os código das duas linguagens que não precisaria se uma pesquisa detalhada. 

### Improving Code Readability Models with Textual Features
### (http://www.cs.wm.edu/~denys/pubs/ICPC%2716-Readability.pdf)

O artigo “Improving Code Readability Models with Textual Features” tenta focar na parte textual para melhorar os modelos que focam em quantidade de caracteres e linhas, já que Ruby tem foco em ser uma linguagem que se assemelha a linguagem natural, eu imagino que nessa parte Ruby teria vantagem. 

Esse artigo usou 600 snippets de código que foram avaliados manualmente (em termos de legibilidade) por mais de 5 mil pessoas, então ele é similar ao artigo que eu usei em relação ao método que foi usado para obter dados, mas em uma escala muito maior são mais de 3 milhões de teste no total segundo o artigo.

Esse artigo contradiz o artigo que eu usei dizendo que comentários desempenham um papel crucial na compreensão do programa e na qualidade do software, uma vez que os desenvolvedores expressam o conhecimento do domínio nos comentários. Eu acredito que isso seja importante na hora de avaliar o programador, mas na hora de comparar uma linguagem com outra, creio que devemos abstrair o fator “programador” se queremos avaliar a linguagem pelo que ela é.

Esse artigo contradiz o que eu usei dizendo que identificadores contendo palavras completas são mais compreensíveis do que identificadores compostos de abreviações, isso baseado no seguinte artigo: “Syntactic identifier conciseness and consistency" (https://www.researchgate.net/publication/220703743_Syntactic_Identifier_Conciseness_and_Consistency).

Todos os trabalhos que eu citei foram feitos de forma empírica, ou seja se baseando na experiência de um conjunto de pessoas, então acho que isso mostra que as pesquisas realizadas se aplicam ao grupo que passou pelo experimento, não necessariamente se aplica a outros programadores, acho que isso é um fato importante sobre as pesquisas de legibilidade e é algo que foi citado no artigo que eu usei.

### A General Software Readability Model
### (https://web.eecs.umich.edu/~weimerw/students/dorn-mcs-paper.pdf)

A principal vantagem desse artigo para esse trabalho é que o modelo descrito neste artigo aborda múltiplas linguagens.

As métricas de legibilidade são bem estabelecidas no domínio da linguagem natural não relacionada ao software. Métricas como o Índice de Legibilidade Automatizado e o Nível de Grau Flesch-Kincaid são comumente usadas em software comercial. Em contraste, modelos descritivos gerais de legibilidade de software são relativamente recentes, propostos inicialmente por Buse. (Essa é uma citação do artigo que eu usei.)

Como uma das ideias primárias na concepção de Ruby era fazer com que Ruby fosse o mais próximo à linguagem natural, eu comecei essa pesquisa achando que um teste de legibilidade de linguagem natural iria demonstrar a legibilidade de Ruby, mas não achei base científica para suportar isso.


### A simpler model of software readability
### (https://www.researchgate.net/publication/221657129_A_simpler_model_of_software_readability)

Também li esse artigo, não cheguei a escrever nada sobre ele, mas acho que é importante para a discussão sobre legibilidade 



## Conclusão

Apesar de Legibilidade de código não ser uma assunto recente, a proposta de um modelo formal para calcular legibilidade é relativamente recente, existem muitas propostas promissoras, mas nem uma automatizada ainda, a proposta de Dorn (A General Software Readability Model) é a mais promissora na minha opinião e é bem objetiva, analisar aspectos visuais, textuais, espaciais e de alinhamento, leva em conta linguagens diferentes, indentação e até os highlights da IDE, mas nesses artigos eu não achei uma forma objetiva de calcular legibilidade sem fazer um pesquisa mais minuciosa, a forma que eu usei para comparar as linguagens é uma interpretação pessoal do artigo “Learning a Metric for Code Readability”, eu espero que a forma que eu usei dê uma ideia básica de como Ruby se compara com PHP em relação a legibilidade.
