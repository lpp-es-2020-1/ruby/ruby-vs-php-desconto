# Ruby vs PHP - [Desconto](https://turing.inf.ufg.br/mod/forum/discuss.php?d=1869)

#### Para executar via docker basta basta abrir um terminal na pasta onde os arquivos estão,<br/>executar o primeiro comando para construir a imagem e o segundo para executar o código.

##### comandos:
```bash
docker build -t desconto .
docker run -it desconto
```
O arquivo **"geraArray.rb"** é o código que eu usei para gerar o **"array_gen.csv"**, criei um CSV com 50 arrays mas é possivel gerar mais.<br/>O resultado está escrito no arquivo **"resultados.md"**.<br/>
Eu escrevi minhas observações sobre legibilidade no arquivo: **"comparando legibilidade.md"**.
